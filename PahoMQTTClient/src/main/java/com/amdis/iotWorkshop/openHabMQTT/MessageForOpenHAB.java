package com.amdis.iotWorkshop.openHabMQTT;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * @author Roland
 * @version 0.1
 * 
 * This MQTT Client program is based on the eclipse Paho project example. There are some 
 * minor modifications for the openHAB example 
 * @see http://www.eclipse.org/paho/clients/android/sample/
 * @see https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttMessage.html
 *
 */
public class MessageForOpenHAB {

	private static final int MAX_MESSAGES = 10000; // Max. number of messages to be sent to the MQTT borker
	
	public static void main_(String[] args) throws InterruptedException {

		MqttClient sampleClient = null;
		MqttMessage message = null;
		MqttTopic topicObj = null;
		
		String topic = "corridorTemp";
		String content = "Message from MQTTClientSample Program";
		int qos = 2; // Quality of service, 2 = persist msg, two-phase acknowledgement across the network
		String broker = "tcp://127.0.0.1:1883"; // Address of the MQTT broker, e.g. Apache Apollo
		String clientId = "MQTTClientForOpenHAB";
		MemoryPersistence persistence = new MemoryPersistence();

		try {
			sampleClient = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			connOpts.setUserName("admin");
			connOpts.setPassword("password".toCharArray());
			System.out.println("Connecting to broker: " + broker);
			sampleClient.connect(connOpts);
			System.out.println("Connected");
			topicObj = sampleClient.getTopic("mqtt");
			System.out.println("Publishing message: " + content);
			for (int i = 0; i < MAX_MESSAGES; i++) {
				content = Integer.toString(i);
				message = new MqttMessage(content.getBytes());
				message.setQos(qos);
				sampleClient.publish(topic, message);
				System.out.println("Message published: " + i);
				Thread.sleep(5000); // sleep for 5 s
			}
			sampleClient.disconnect();
			System.out.println("Disconnected");
			System.exit(0);
		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		} finally {
			try {
				sampleClient.disconnect();
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			System.out.println("Disconnected");
			System.exit(0);
			
		}
	}
}