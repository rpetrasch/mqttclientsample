package com.amdis.iotWorkshop.client.pahoMQTTClient;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttTopic;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

/**
 * @author Roland
 * @version 0.1
 * 
 *          This MQTT Client program is based on the eclipse Paho project
 *          example. There are some minor modifications for the Node-Red example
 * @see http://www.eclipse.org/paho/clients/android/sample/
 * @see https://www.eclipse.org/paho/files/javadoc/org/eclipse/paho/client/mqttv3/MqttMessage.html
 *
 */
public class MqttPublishSample {

	private static final int MAX_MESSAGES = 100; // Max. number of messages to
													// be sent to the MQTT
													// borker

	public static MqttClient sampleClient = null;
	public static MqttMessage message = null;
	public static MqttTopic topicObj = null;

	public static String topic = "MQTT";
	public static String content = "Message from MQTTClientSample Program";
	public static int qos = 2; // Quality of service, 2 = persist msg, two-phase
								// acknowledgement across the network
	public static String broker = "tcp://127.0.0.1:1883"; // Address of the MQTT
															// broker, e.g.
															// Apache Apollo
	public static String clientId = "MQTTClientSample";
	public static MemoryPersistence persistence = new MemoryPersistence();

	public static void main_(String[] args) throws MqttException {
		sampleClient = connect();
		for (int i = 0; i < MAX_MESSAGES; i++) {
			sendMessage(sampleClient, topic, content);
		}
		sampleClient.disconnect();
		System.out.println("Disconnected");
		System.exit(0);

	}

	public static void sendMessage(MqttClient client, String topic, String content) {
		try {
			topicObj = client.getTopic(topic);
			System.out.println("Publishing message: " + content);
			message = new MqttMessage(content.getBytes());
			message.setQos(qos);
			client.publish(topic, message);
			System.out.println("Message published");
		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		} 
	}

	public static MqttClient connect() {
		MqttClient client = null;
		try {
			client = new MqttClient(broker, clientId, persistence);
			MqttConnectOptions connOpts = new MqttConnectOptions();
			connOpts.setCleanSession(true);
			connOpts.setUserName("admin");
			connOpts.setPassword("password".toCharArray());
			System.out.println("Connecting to broker: " + broker);
			client.connect(connOpts);
			System.out.println("Connected");
		} catch (MqttException me) {
			System.out.println("reason " + me.getReasonCode());
			System.out.println("msg " + me.getMessage());
			System.out.println("loc " + me.getLocalizedMessage());
			System.out.println("cause " + me.getCause());
			System.out.println("excep " + me);
			me.printStackTrace();
		}
		return client;
	}

}