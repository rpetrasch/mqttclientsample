package com.amdis.iotWorkshop.client.pahoMQTTClient;

import java.io.*;
import java.net.*;
import java.util.Optional;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class HTPPClientForNodeMCU {

	public static void main(String[] args) {

		MqttClient client = MqttPublishSample.connect();
		Optional<String> msg;

		try {
			for (int i = 0; i < 100; i++) {
				URL root = new URL("http://192.168.1.36");
				URLConnection rootConnection = root.openConnection();
				readAndPrintFromURLConnection(rootConnection);

				URL temp = new URL("http://192.168.1.36/temp");
				URLConnection tempConnection = temp.openConnection();
				msg = Optional.ofNullable(readAndPrintFromURLConnection(tempConnection));
				MqttPublishSample.sendMessage(client, "/NodeMCU/temp", msg.orElse("NULL"));

				URL humidity = new URL("http://192.168.1.36/humidity");
				URLConnection humidityConnection = humidity.openConnection();
				msg = Optional.ofNullable(readAndPrintFromURLConnection(humidityConnection));
				MqttPublishSample.sendMessage(client, "/NodeMCU/humidity", msg.orElse("NULL"));
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {

			try {
				client.disconnect();
			} catch (MqttException e) {
				e.printStackTrace();
			}
			System.out.println("Disconnected");
			System.exit(0);
		}
	}

	public static String readAndPrintFromURLConnection(URLConnection urlConnection) throws Exception {
		BufferedReader in = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
		String inputLine;
		String lastInputLine = null;
		while ((inputLine = in.readLine()) != null) {
			System.out.println(inputLine);
			lastInputLine = inputLine;
		}
		in.close();
		return lastInputLine;
	}

	public void writer() throws Exception {
		URL url = new URL("http://192.168.1.37:80/temperature");
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setDoOutput(true);
		PrintWriter out = new PrintWriter(connection.getOutputStream());
		String name = "name=" + URLEncoder.encode("myname", "UTF-8");
		String email = "email=" + URLEncoder.encode("email@email.com", "UTF-8");
		out.println(name + "&" + email);
		out.close();
		BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String line;
		while ((line = in.readLine()) != null) {
			System.out.println(line);
		}
		in.close();
	}

}
