#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>
#include <string>  
#include <DHT.h>
#define DHTTYPE DHT11
#define SENSOR_PIN D4
#define LED_PIN D1 // 13;

DHT sensor_dht(SENSOR_PIN, DHTTYPE, 15);

int humidity = 0;
int temp = 0;

const char* WIFI_SSID = "YOUR WIFI SERVER";
const char* WIFI_PASSWORD = "YOUR WIFI PASSWORD";
#define WEBSERVER_PORT 80

ESP8266WebServer webServer(WEBSERVER_PORT);


void handleRoot() {
  digitalWrite(LED_PIN, 1);
  webServer.send(200, "text/plain", "NodeMCU ESP8266: Webserver is running.");
  digitalWrite(LED_PIN, 0);
}

void handleNotFound(){
  digitalWrite(LED_PIN, 1);
  String message = "File Not Found\n\n";
  message += "URI: ";
  message += webServer.uri();
  message += "\nMethod: ";
  message += (webServer.method() == HTTP_GET)?"GET":"POST";
  message += "\nArguments: ";
  message += webServer.args();
  message += "\n";
  for (uint8_t i=0; i < webServer.args(); i++){
    message += " " + webServer.argName(i) + ": " + webServer.arg(i) + "\n";
  }
  webServer.send(404, "text/plain", message);
  digitalWrite(LED_PIN, 0);
}

void setup(void){
  sensor_dht.begin();
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, 0);
  Serial.begin(115200);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("");

  // Wait for connection
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to ");
  Serial.println(WIFI_SSID);
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());

  if (MDNS.begin("esp8266")) {
    Serial.println("MDNS responder started");
  }

  webServer.on("/", handleRoot);

  webServer.on("/temp", [](){
    webServer.send(200, "text/plain", String(temp));
  });
  webServer.on("/humidity", [](){
    webServer.send(200, "text/plain", String(humidity));
  });

  webServer.onNotFound(handleNotFound);

  webServer.begin();
  Serial.println("HTTP server started");
}

void loop(void){
  int read = sensor_dht.read();
  humidity = sensor_dht.readHumidity();
  temp = sensor_dht.readTemperature();

  webServer.handleClient();
}