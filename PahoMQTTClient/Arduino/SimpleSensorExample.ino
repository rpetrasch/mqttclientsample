#include <DHT.h>
#define DHTTYPE DHT11
#define DHTPIN D4
DHT dht(DHTPIN, DHTTYPE, 15);

void setup() {
  dht.begin();
  Serial.begin(115200);
}

void loop() {
  int rd = dht.read();
  int hum = dht.readHumidity();
  int temp = dht.readTemperature();
  char str[] = "";
  sprintf(str, "read: %d  hum: %d  temp: %d", rd, hum, temp);
  Serial.println(str);
  delay(100);
}