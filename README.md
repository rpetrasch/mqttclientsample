![IoT.JPG](https://bitbucket.org/repo/jABqRG/images/2283128895-IoT.JPG)
# README #

THis is the repo for the eclipse MQTT client Java program

### What is this repository for? ###

* Use it with the IoT workshop
* Version 0.1
* [Learn Markdown: SET IoT workshop event](http://www.meetup.com/Software-Engineering-Thailand/events/232801024/)

### How do I get set up? ###

* Clone the repo in eclipse
* Set up Apollo and Node-Red as described in the workshop tutorial
* Dependencies: eclipse Paho

### Who do I talk to? ###

* Repo owner: Roland roland.petrasch@gmail.com